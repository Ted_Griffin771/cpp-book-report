## Day1:白兔

煢煢白兔，東走西顧。衣不如新，人不如故。[心理學研究#懷舊](https://facebook.com/IncredivilleTW/photos/a.1780902738892001/2774128149569450/?type=3&source=48)，會讓人變得積極樂觀，本來想讀VB重溫16歲小高一時第一次寫出貪食蛇遊戲的感動，但發現VBA還停在VB6覺得還是退而求其次讀完大一的C++螞蟻書5e好了。基本上5e是2004年出版採用C99標準。而且C++語言只適合寫系統，跟業務邏輯(backend)沒有關係，所以這篇只是一個七年級工程師絮絮叨叨的自我救贖。

* 螞蟻書範例: https://cs.fit.edu/~mmahoney/cse1502/examples/
* 其他範例就直接附在內文

## day2
rules of operator precedence 很簡單的小概念就是運算子是有階級的，就像先乘除後加減，如要先加減就必須小括號包起來，從這個例子就可知道小括號階級大於乘除大於加減，但螞蟻書開頭就講這個當然不只是為了四則計算，小學畢業都知道四則運算規則，一個原因在C++裏頭指標的符號*跟乘法長的一樣舉個例子: 

```
#若要取第三個元素即v[2]要用括號包起來 : *(vPtr + 2),不然因為*號階級高於+號就會變v[0]+2

int main(){

    int v[5] = {100,200,300,400,500}; 
    int *vPtr = v ;  //直接將陣列v丟給指標
    cout << vPtr  << endl ;               //3000
    cout << *(vPtr + 2)  << endl ;        //300
    cout << *vPtr + 2  << endl ;          //102
    cout <<  vPtr  + 2  << endl ;         //3016
    cout << ++vPtr  << endl ;             //3008，騷操作
    return 0;

}
```

## day3
英文C++ How To Program中文翻的文謅謅名為"程式設計的藝術"作為傳奇"初階"書籍CS101曾經的御用教材開頭不廢話直接介紹物件導向當然在13章還會有更深入的探討，C++的類別有三個關鍵字public, protect與private，public代表公開的其中會定義一些可以在類別外部或子類別自由使用的方法或資料成員，在C++中類別的方法英文還是function，但為了方便區別，中文翻成跟JAVA一樣的Method(方法)，類別中第一個定義的通常是建構式Contractor負責初始化"物件"，與類別同名不須定義返回值是其重要特徵，範例中的建構式有一個參數name並呼叫公開成員方法setCourseName，setCourseName受到建構式呼叫取得name參數會將private底下的私有資料成員courseName設為name。

由於courseName在private底下只有該class的Method才能讀取這就實現封裝，本篇範例寫了一大票程式碼就只是為了實現一個功能print(book_name)。
```
// Fig. 3.7: fig03_07.cpp
#include <iostream>
using std::cout; 
using std::endl;
using std::string;

// GradeBook class definition
class GradeBook
{
public:
   // constructor initializes courseName with string supplied as argument
   GradeBook( string name )
   {
      setCourseName( name ); // call set function to initialize courseName
   } // end GradeBook constructor

   // function to set the course name
   void setCourseName( string name )
   {
      courseName = name; // store the course name in the object
   } // end function setCourseName

   // function to get the course name
   string getCourseName()
   {
      return courseName; // return object's courseName
   } // end function getCourseName

   // display a welcome message to the GradeBook user
   void displayMessage()
   {
      // call getCourseName to get the courseName
      cout << "Welcome to the grade book for\n" << getCourseName()  
         << "!" << endl;
   } // end function displayMessage
private:
   string courseName; // course name for this GradeBook
}; // end class GradeBook  

// function main begins program execution
int main()
{
   // create two GradeBook objects
   GradeBook gradeBook1( "CS101 Introduction to C++ Programming" );
   GradeBook gradeBook2( "CS102 Data Structures in C++" );

   // display initial value of courseName for each GradeBook
   cout << "gradeBook1 created for course: " << gradeBook1.getCourseName()
      << "\ngradeBook2 created for course: " << gradeBook2.getCourseName() 
      << endl;
   return 0; // indicate successful termination
} // end main
```

## day4
昨天把所有程式碼都寫在一起包括class與程式進入點main()，但這樣做有個缺點就是會暴露原始碼, C/C++算是一門很老的語言當時的風氣傾向保密細節只留下.h標頭檔展示類別的介面(interface of class)如下GradeBook.h，而實作會放在.cpp檔中先編譯好，這樣就可以避免使用者看到實作細節。

```
//GradeBook.h
#include <string> // class GradeBook uses C++ standard string class
using namespace std;

class GradeBook
{
public:
   GradeBook( string ); // constructor that initializes courseName
   void setCourseName( string ); // function that sets the course name
   string getCourseName(); // function that gets the course name
   void displayMessage(); // function that displays a welcome message   
   
private:
   string courseName; // course name for this GradeBook
}; 
```

```
//GradeBook.cpp
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook
using namespace std;

// constructor initializes courseName with string supplied as argument
GradeBook::GradeBook( string name )
{
   setCourseName( name ); // validate and store courseName
} 

void GradeBook::setCourseName( string name )
{
	
   if ( name.length() <= 25 ) // if name has 25 or fewer characters
      courseName = name; // store the course name in the object

   if ( name.length() > 25 ) // if name has more than 25 characters
   { 
      // set courseName to first 25 characters of parameter name
      courseName = name.substr( 0, 25 ); // start at 0, length of 25

      cout << "Name \"" << name << "\" exceeds maximum length (25).\n"
         << "Limiting courseName to first 25 characters.\n" << endl;
   } // end if
} 

string GradeBook::getCourseName()
{
   return courseName; // return object's courseName
} 

void GradeBook::displayMessage()
{
   // call getCourseName to get the courseName
   cout << "Welcome to the grade book for\n" << getCourseName()  
      << "!" << endl;
} 
```

## day5 如何超越Google
今天也來討論一個科幻問題現在一台家用的Home Lab的處理能力是否超過當年Google的Date Center，眾所周知Google的三駕馬車GFS、MapReduce、Bigtable最早發表於2003年，那時Google號稱它的Date Center已經有破千台Server，出於方便計算假設全部的伺服器全採用Supermicro X6DH8使用Intel Nocona Xeon單核處理器(2004推出)，因為是雙路主機板(可以插兩個CPU)故整體跑分提高為431分，但分散式系統最大的問題是IO，當年的記憶體,硬碟,網路可是慢到靠杯，同時IO屬於特權指令導致程式必須在User Mode與Kernel Mode反覆切來切去，而且記憶體在北橋但網路與硬碟在南橋所以在整個分散式系統中單台機器能發揮的效能給它打個5折好了得215.5分。但考量當時IBM的大型主機 or 超級電腦價格昂貴，以Google的規模使用Intel x86白菜伺服器組分散式系統就算單台機器只能發揮出3成效能，從財務的角度出發也是很划算的所以Paper一發表,Yahoo!就跟著做了一個類似的系統名為Hadoop, HDFS, MapReduce。

2021/10為止在[Geekbench](https://browser.geekbench.com/v5/cpu/multicore)上跑分最高的是AMD EPYC的7萬5千分，OS選ubuntu 20.04配上GIGABYTE R282當Home Lab剛剛好，微軟的小算盤拿出來按一下，7萬5除215.5得348而且現在還有NVMe, SSD, DDR5, PCIe 4/5 , GPU(早期的GPU只能做影像處理沒有甚麼科學計算與數據處理能力，畢竟CPU裡除了計算單元ALU還有控制單元CU) 加上不用跑網路自然不會有封包碰撞,撞碰撞偵測與處理, 路由, 網路設備故障等一拖拉庫的問題，也就是說現在一台Home LAB的處理能力確實有可能跟2004年Google的Date Center的處理能力一較高下何況1台Home Lab不夠可以買3台呀，只要機器都屬於同一個物理二層網路效能耗損與延遲應該不會太大，再掛個Load Balance或做HA簡直美滋滋。最後這就是一篇科幻文完全張飛打張菲，喔說錯是張飛打岳飛。

終於知道為什麼Python等了20年才稱王，以當時的CPU用Python算MSE(均方誤差)，還是人工打卡西歐計算機好了。

## day6
最後就會形成下面這張圖，左邊工程師負責寫類別並compile，右邊負責寫主程式並compiler，中間用介面當橋樑，最後藉由連結器拉再一起(linker)成可執行檔交給User使用，三方都不知道對方的如何寫的只看得到標頭檔.h。不可否認這種設計有點古老了，其實根本沒人有興趣看你我的程式碼，這世界很少靠黑科技賺錢的公司大部分是靠商業模型賺錢(請君看看那可憐的世界前三大數據公司MapR只賣了5000萬美金而另外兩家直接被打到合併，股價一度從21跌到5元同期NASDAQ都從6000漲到16000，最終乘著2021景氣好趕緊把公司賣掉)，所以最新的C++20與之後的標準可以不用標頭檔.h直接import package

接續Day4介面與類別實作細節都有了還缺一個主程式，為了隱藏實作細節主程式只引入GradeBook.h標頭檔
```
// Fig. 3.17: fig03_16.cpp
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook
using namespace std;

int main()
{
   // create two GradeBook objects; 
   // initial course name of gradeBook1 is too long
   GradeBook gradeBook1( "CS101 Introduction to Programming in C++" );
   GradeBook gradeBook2( "CS102 C++ Data Structures" );

   // display each GradeBook's courseName 
   cout << "gradeBook1's initial course name is: " 
      << gradeBook1.getCourseName()
      << "\ngradeBook2's initial course name is: " 
      << gradeBook2.getCourseName() << endl;

   // modify myGradeBook's courseName (with a valid-length string)
   gradeBook1.setCourseName( "CS101 C++ Programming" );

   // display each GradeBook's courseName 
   cout << "\ngradeBook1's course name is: " 
      << gradeBook1.getCourseName()
      << "\ngradeBook2's course name is: " 
      << gradeBook2.getCourseName() << endl;
} 
```


![alt text](./static/d61.png "linker & compiler")


## day7 

昨天有講一個古老的設計:利用標頭檔將類別介面與實作拆開並預先編譯用以隱藏實作細節但還是不夠安全隱密，因為從標頭檔還是可以看到private的資料成員與方法成員，這時可用proxy class代理類別進一步隱藏來看看這好玩的範例。 

第一個文件Implementation.h(10.24)中有我們要隱藏的原始碼，第二個文件Interface.h(10.25)包含proxy class的介面，要注意的是有兩段分別是class Implementation;定義一個Implementation類別，並在private底下宣告一個Implementation類別的指標，第三個文件Interface.cpp(10.26)是proxy class的實作，值得注意的是不僅include了Interface的介面也include了Implementation的程式碼細節。

最後一個文件fig10_27.cpp就是精隨了只include了Interface.h就是代理物件的介面，原始的Implementation類別被徹底隱藏，接著在main()中就是一般的Interface物件方法的操作.getValue()與.setValue()，因為有解構式的存在就不用手動delete回收物件。講完了不知大家有沒有覺得C++真簡單，話說那麼簡單的語言絕對不能只有我學過，我已經成功拉一個成大經濟系的學第入坑了比老鼠會還邪惡ㄏㄏ。

* Fig. 10.24: Implementation.h
```
class Implementation 
{
public:
   // constructor
   Implementation( int v )  
      : value( v ) // initialize value with v
   { 
      // empty body
   } // end constructor Implementation

   void setValue( int v )   
   { 
      value = v; // should validate v
   } // end function setValue

   int getValue() const  { 
      return value; 
   } // end function getValue
private:
   int value; // data that we would like to hide from the client
}; // end class Implementation
```
* Fig. 10.25: Interface.h
```
// Client sees this source code, but the source code does not reveal 
// the data layout of class Implementation.

class Implementation; // forward class declaration required by line 17

class Interface 
{
public:
   Interface( int ); // constructor
   void setValue( int ); // same public interface as
   int getValue() const; // class Implementation has
   ~Interface(); // destructor
private:
   // requires previous forward declaration (line 6)
   Implementation *ptr;   
}; // end class Interface
```
* Fig. 10.26: Interface.cpp
```
// Implementation of class Interface--client receives this file only 
// as precompiled object code, keeping the implementation hidden.
#include "Interface.h" // Interface class definition
#include "Implementation.h" // Implementation class definition

// constructor
Interface::Interface( int v ) 
   : ptr ( new Implementation( v ) ) // initialize ptr to point to
{                                    // a new Implementation object
   // empty body
} // end Interface constructor

// call Implementation's setValue function
void Interface::setValue( int v ) 
{ 
   ptr->setValue( v ); 
} // end function setValue

// call Implementation's getValue function
int Interface::getValue() const 
{ 
   return ptr->getValue(); 
} // end function getValue

// destructor
Interface::~Interface() 
{ 
   delete ptr; 
} // end ~Interface destructor
```


* Fig. 10.27: fig10_27.cpp
```
// Hiding a class’s private data with a proxy class.
#include <iostream>
using std::cout;
using std::endl;

#include "Interface.h" // Interface class definition

int main()
{
   Interface i( 5 ); // create Interface object
 
   cout << "Interface contains: " << i.getValue() 
      << " before setValue" << endl;

   i.setValue( 10 );

   cout << "Interface contains: " << i.getValue() 
      << " after setValue" << endl;
   return 0;
} // end main
```

## day8
第四章也蠻簡單的，開頭說Böhm與Jacopini證明所有程式都可使用三種流程控制表達
1.	執行一個子程式，然後執行下一個（順序）
2.	依照布林變數的結果，決定執行二段子程式中的一段（選擇）
3.	重覆執行某子程式，直到特定布林變數為真為止（迴圈）
簡單的來看就是if else , switch, for, while, do while，while與do while差別只在於一個先做再判斷一個先判斷在做，來改寫一下前面的setCourseName方法讓兩個if變成if..else
```
void GradeBook::setCourseName( string name )
{
   if ( name.length() <= 25 ) // if name has 25 or fewer characters
      courseName = name; // store the course name in the object
   else
   {
      courseName = name.substr( 0, 25 ); // start at 0, length of 25
      cout << "Name \"" << name << "\" exceeds maximum length (25).\n"
         << "Limiting courseName to first 25 characters.\n" << endl;
   }
}
```


## day9

巢狀迴圈沒甚麼值得提的，注意別太多層如果是三層那時間複雜度等於O(n^3)不太優，再聊個遞增運算子，四五章就做結束了，超級大灌水沒辦法啊四五章只是在講if, while, for ,switch，這也不需要特別上課所以曾說大一自修看完半本C++螞蟻書很正常，凡有點進取心的人都辦的到並沒甚麼難的，C語言難的是後面的指標與記憶體操作，難的是當時IDE很弱輔助功能趨近於零,程式出錯要找個半天Dev-C++有跟沒有一樣，而我的另一位老師乾脆連IDE都不用直接開記事本寫程式,寫完開CMD/Terminal編譯,。  

++就是x=x+1的意思，而x++ 與++x的差別只是一個先加在顯示結果，一個先顯示結果在加，範例也直接拿day1的來用，可以看到vPtr++ 顯示的記憶體位置不變而++vPtr記憶體位置往後走了8 Bytes。
```
#include <stdio.h>
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main(){

    int v[5] = {100,200,300,400,500}; 
    int *vPtr = v ;  //直接將陣列v丟給指標
    cout << vPtr++  << endl ;   //3000
    cout << ++vPtr  << endl ;   //3008，騷操作
    return 0;

}
```
<img src="./static/d81.png" alt="d8" width=70%/>


## day10
1. 第六章函數與遞迴，強調的是函式原型(function prototype)又稱為函式宣告(function declaration)會告知編譯氣函式的名稱, 回傳值型別, 參數數目, 參數型別還有最重要的順序，prototype或declaration跟definition的差別可以參考wiki提供的範例。    
```
#include <stdio.h>
int MyFunction(int n);  /* Prototype or declaration */

int main( void )  /* Calling function */{
  printf("%d\n", MyFunction(6));  /* Error: forgot argument to MyFunction */
  return 0;
}

int MyFunction( int n )  /* Called function definition */{
  if (n == 0){
    return 1;
  }
  else{
    return n * MyFunction(n - 1);
  }
}
```
2. 強制轉型(coercion of argument)提到在C++中類型眾多short, int , float, double, unsigned int, 下往上轉當然沒問題只是多花記憶體空間但上往下轉比如: double轉成int或轉成unsigned int就可能造成資料丟失問題。    

3. 一般說我們說bool類型的值非True即Flase，int類型的值介於-2,147,483,648 至 2,147,483,647，但我們也可以使用enum自行定義類型例如月份: enum Months { JAN=1, FEB, MAR, APR, MAY, JUN, AUG, SEP, OCT, NOV, DEC};

4. 三個儲存關鍵字auto, register, static，函式中的變數預設是auto當函式存在時存在，函式結束時根據最小授權原則principle of privilege會自動收回記憶體空間，以現今的語言來說C++已經是運行時最快的但在一些追求極致速度的系統中例如:交易系統C++還有一個關鍵字register可將變數存放於CPU的暫存器(register)不僅增加讀取速度更節省變數在register與memory間的反覆傳輸，運算速度整個Level Up，當然CPU的暫存器是彌足珍貴的所以這是一種建議性質關鍵字當CPU的暫存器空間不足還是會被丟回memory。

5. 預設情況下當函數生命週期結束變數也會跟著一起被回收，如果想要變數生存在整個程式執行的期間就可以使用全域變數(Global var)，然全域變數是所有函數都可以讀取，全域變數滿天飛常常是造成問題的根源，比較好的方式對區域變數使用static關鍵字進行最小授權。值得注意的是static用在全域變數或函式名稱時有不同用意。

6. 有句話說「遞迴（recurse）只應天上有，凡人該當用迴圈（iterate）」，確實比起迴圈遞迴的使用更加困難，但也有些好處
* 節省記憶體空間  
* "通常"比較快  
* 但要小心遞迴次數免得stack overflow  

反正像我這種凡人只有在研究所考試時寫河內塔, 求最大公因數等經典題目，工作後就很少遇到了就不多做介紹。


## days11
奇書共賞第十天，可惜螞蟻書已經不再更新不然許多地方講得很不錯，但現在已經不是1980年而是2020年CS101老早就把課程語言換成Python，今天來看看範例6.12

範例第一段意思大概是這樣，當全域或Main函數與Main函數裡的New Scop都宣告一個變數x時，並不會產生錯誤它們雖然同名但卻是不同變數有各自的記憶體位置，在當程式運行到New Scop時會顯示New Scop裡的x=7, 回到Main函式則會顯示x=5
* Fig. 6.12: fig06_12.cpp
```
#include <iostream>
using std::cout;
using std::endl;

int x = 1; // global variable

int main()
{
   int x = 5; // local variable to main

   cout << "local x in main's outer scope is " << x << endl;
   cout << &x << endl;
   { // start new scope
      int x = 7; // hides x in outer scope
      
      cout << "local x in main's inner scope is " << x << endl;
      cout << &x << endl;
   } // end new scope

   cout << "local x in main's outer scope is " << x << endl;

} // end main

```


##  day12
繼續看範例fig06_12.cpp下半段，一開始定義三個函式原型void useLocal(), void useStaticLocal(), void useGlobal(), 我稍微修改了一下main(),透過for迴圈各自呼叫三次，可以發現useStaticLocal()與useGlobal()的變數x的值各累加了兩次，但 useLocal()裡x的值並沒有改變。
* Result 
```
local x is 100
local static x is 200
global x is 300

local x is 100
local static x is 201
global x is 301

local x is 100
local static x is 202
global x is 302
```
* fig06_12.cpp
```
#include <iostream>
using std::cout;
using std::endl;

void useLocal(); // function prototype
void useStaticLocal(); // function prototype
void useGlobal(); // function prototype

int x = 300; // global variable

int main()
{
   for (int i = 0; i <3; i++)
   {
      useLocal(); // useLocal has local x
      useStaticLocal(); // useStaticLocal has static local x
      useGlobal(); // global x also retains its value
      cout << endl;
   }

   return 0; // indicates successful termination
} // end main

// useLocal reinitializes local variable x during each call
void useLocal( void )
{
   int x = 100; // initialized each time useLocal is called
   
   cout << "\nlocal x is " << x ;
   x++;
} 

void useStaticLocal( void )
{
   static int x = 200; // initialized first time useStaticLocal is called 

   cout << "\nlocal static x is " << x ;
   x++; 
} 

void useGlobal( void )
{
   cout << "\nglobal x is " << x ;
   x++; 
} 

```

## day13

function stack，stack 是一種資料結構屬於後進先出，如下圖記體體空間簡單的來說第一塊擺全域變數靜態變數與程式碼，第二塊Heap擺動態變數，第三塊stack擺程式呼叫，一開始計算機會先執行main()接著創造新Foo物件*f 再呼叫Foo的成員方法setValue()將成員屬性m_value設為g_default_value，接著stack會按著程式呼叫順序的反方向回收記憶體，先回收f->setVaue()再回收main()，然後C++是沒有GC的語言一般的函式參數因為default keyword is auto所以隨著函示結束也會跟著回收，但物件Foo不是main函式的參數所以要主動回收delete f。

<img src="./static/d12.png" alt="d12" width=70%/>

## day14
域解析運算子:: 真不知道該怎麼形容它螞蟻書也寫得很玄，不如直接看範例,在main函式中print number會顯示10.5如果想要使用global number就必須使用::解析域運算元去指定現在要使用的命名空間，反正就是這樣。

```
// Fig. 6.23: fig06_23.cpp
// Using the unary scope resolution operator.
#include <iostream>
using std::cout;
using std::endl;

int number = 7; // global variable named number

int main()
{
   double number = 10.5; // local variable named number

   // display values of local and global variables
   cout << "Local double value of number = " << number
      << "\nGlobal int value of number = " << ::number << endl;
   return 0; // indicates successful termination
} // end main

void set_varible(){

   short number = 7

}

```


## day15
函式多載(overloading)，今天順著螞蟻書的順序先討論函式多載(overloading)與樣板(Template),這段英文講得很透徹甚麼是多載Overloading allows different function to have the same name, 本質就是除了名稱其他都是不同的，如返回值/參數類型/參數數量/程式邏輯，下面個範例第一個print函數沒有回傳值接收int當參數，第二個print函數有回傳值double並接受double當參數，第三個print函數接受兩個參數:其一指標常數字元其二一個控制內部迴圈的參數int，同時這三個print函數內部邏輯完全不同，這就是多載的精髓"只有名稱是一樣的其他都不一樣",最終結果如下圖二。

```
#include <iostream>
using namespace std;
 
void print(int i) {
   cout << " Here is int " << i << endl;
}
double print(double  f) {
   return f;
}
void print(char const *c, int j) {
   for (int i = 0; i < j; i++){
      cout << " Here is char* " << c << endl;
   }
}
 
int main() {
  print(10);
  cout << " Here is float " << print(10.0) << endl;
  print("ten",2);
  return 0;
}
```
* Result
```
Here is int 10
Here is float 10
Here is char* ten
Here is char* ten
```


## day16
再來說樣板template，樣板只有參數型態不一樣其餘都相同(包括程式邏輯),樣板基本上與寫一般的函式沒甚麼差別，但變數型態並不指定而是通常由一個名為正規型別引數(Formal type parameter)的T取代如下螞蟻書範例:
```
T maximum( T value1, T value2, T value3 ){

   T maximumValue = value1; // assume value1 is maximum

   // determine whether value2 is greater than maximumValue
   if ( value2 > maximumValue )
   maximumValue = value2;

   // determine whether value3 is greater than maximumValue
   if ( value3 > maximumValue )
   maximumValue = value3;

   return maximumValue;

} // end function template maximum 
```
程式邏輯非常簡單傳進來三個value，首先將maximumValue設為value1再讓value2與maximumValue比大小如果value2大於maximumValue則將maximumValue設為value2，接著對value3重覆相同邏輯，跟一般函式的差別只有型態用T代替，當然這樣去run編譯器會直接報錯因為從頭到尾沒講T是啥? 所以開頭要補這一段template < class T >或是 template< typename T >完整程式碼如下在main()函數中要比哪種型態都可以，不知諸位是不是充分體驗到C++的樂趣呢?那麼簡單的語言被CS101換掉當然是有原因的，C++還有更風騷的操作overloading + template + static，砸再一起做撒尿牛丸一整個花枝招斬，那麼好的語言絕對不能只有我學過!喔對了還有一個泛型(generic)沒說。
```
// Fig. 6.26: maximum.h
// Definition of function template maximum. 
template < class T > // or template< typename T >
T maximum( T value1, T value2, T value3 ){

   T maximumValue = value1; // assume value1 is maximum

   // determine whether value2 is greater than maximumValue
   if ( value2 > maximumValue )
   maximumValue = value2;

   // determine whether value3 is greater than maximumValue
   if ( value3 > maximumValue )
   maximumValue = value3;

   return maximumValue;

} // end function template maximum 

```

```
// Fig. 6.27: fig06_27.cpp
// Function template maximum test program.
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "maximum.h" // include definition of function template maximum

int main()
{
   // demonstrate maximum with int values
   int int1, int2, int3;

   cout << "Input three integer values: ";
   cin >> int1 >> int2 >> int3;

   // invoke int version of maximum
   cout << "The maximum integer value is: "
      << maximum( int1, int2, int3 );        

   // demonstrate maximum with double values
   double double1, double2, double3;

   cout << "\n\nInput three double values: ";
   cin >> double1 >> double2 >> double3;

   // invoke double version of maximum
   cout << "The maximum double value is: "
      << maximum( double1, double2, double3 );

   // demonstrate maximum with char values
   char char1, char2, char3;

   cout << "\n\nInput three characters: ";
   cin >> char1 >> char2 >> char3;

   // invoke char version of maximum
   cout << "The maximum character value is: "
      << maximum( char1, char2, char3 ) << endl;
   return 0; // indicates successful termination
} // end main
```

## day17
arrary這東西很別好理解，一維arrary就是線，二維arrary就是面，三維arrary就是立方體，四維arrary的BigO($n^4$)所以跟迴圈一樣盡量不要超過3維，而且四維以上空間也不容易理解，這章實在沒甚麼好說的何況我手上拿的是螞蟻書5e 2004年出版標準大概使用c99(1999制定)可能很多細節跟c20(2020年制定)不同，於是我們就跳脫書本內容比較C++ array 與python的list

C++ array
1.	長度不可改變
2.	連續記憶體空間
3.	只能透過創建新array並將舊array複製到新array的方式更改長度
4.	所有元素的型別都必須相同
5.	Array不可互相比較

Python list 
1.	長度可隨意增加
2.	每個元素的型別隨意，除了基本型別甚至可以擺tuple, set, dict, object
3.	因為上述性彈性導致速度超慢所以Pandas的array其實是由C++實現

因為C++原始的array一開始就必須宣告大小並配置記憶體空間，一不小心超過array就造成錯誤，所以創建了一個"樣版類別(Class) vector"，他是一個樣板Template所以可以是任何型別，同時他是個類別(Class)所以有些方法(method)可以使用例如size(), empty(), at(), begin(), end() ，也可以輕易的對兩個vector使用運算元< > = =! 。然後螞蟻書的範例就不解說了。



##  day18
傳值與傳址，但C++是一個特別的語言比C語言更複雜真要細分可分成3類(傳值,傳址,傳參考)，同時指標還可以與常數const並用比如: 指向常數資料的常數指標const int *const ptr =&x; 真是太神奇了捷克，有個笑話說當你想要朋友放棄學程式的方法就是叫他去學C++保證三個月內見效，言歸正傳螞蟻書只是一本入門書而且現在主流還是Java, C, GO，所以本次只複習最簡單的指標範例:  
```
#include <iostream>
using std::cout;
using std::endl;

void cubeByAddress( int * ); // prototype

int main () {

    int number =5 ;
    cout << "Origin value is "<< number << endl;
    cubeByAddress( &number); //pass address to cubeByAddress
    cout << "The new number is " << number << endl;

}

void cubeByAddress( int *nPtr ) {

    *nPtr = *nPtr * *nPtr * *nPtr;  //*nPtr = (*nPtr) * (*nPtr) * (*nPtr);
    
}
```
這真是超棒範例，一堆星星，第一個cubeByAddress( &number)中的&number是number的記憶體位址傳入cubeByAddress函數中，函數void cubeByAddress( int \*nPtr ) 定義它有一個指標參數\*nPtr，\*nPtr的值value就是number的記憶體位址也就是&number

最後\*nPtr花式連乘三次\*nPtr * \*nPtr * \*nPtr後再設還給*nPtr，cubeByAddress是void沒有回傳但靠著傳址將number改成了125，這就是最簡單的傳址示範，別忘了真正的C++還有很多變化讓人馬上想改學其他程式語言真是很棒呢!   


##  day19
繼續講\*指標，指標不是只能有一個還能繼續層層指下去
```
#include <stdio.h>
#include <stdlib.h>

int main()
{

    int p = 10;
    int *ptr = &p;
    int **ptr2 = &ptr;
    int ***ptr3 = &ptr2;

    printf("取得p的記憶體位置: %p\n", &p);
    printf("取得p值: %d\n", p);

    printf("*ptr取得到的p值: %d\n", *ptr);
    printf("ptr指向的記憶體位址: %p\n", ptr);
    printf("取得ptr自身的記憶體位址: %p\n\n\n", &ptr);
 
    printf("**ptr2取得到的p值:%d\n", **ptr2);
    printf("*ptr2指向p記憶體位置:%p\n", *ptr2);
    printf("ptr2指向ptr1的記憶體位置:%p\n", ptr2);
    printf("取得ptr2記憶體位址:%p\n\n\n", &ptr2);

    printf("***ptr3取得到p的值:%d\n", ***ptr3);
    printf("**ptr3指向p記憶體位置:%p\n", **ptr3);
    printf("*ptr3指向ptr1的記憶體位置:%p\n", *ptr3);
    printf("ptr3指向ptr2的記憶體位置:%p\n", ptr3);
    printf("取得ptr3記憶體位址:%p\n", &ptr3);
    return 1;
}
```

```
// result
取得p的記憶體位置: 0x7fffffffdb0c
取得p值: 10

*ptr取得到的p值: 10
ptr指向的記憶體位址: 0x7fffffffdb0c
取得ptr自身的記憶體位址: 0x7fffffffdb10

**ptr2取得到的p值:10
*ptr2指向p記憶體位置:0x7fffffffdb0c
ptr2指向ptr1的記憶體位置:0x7fffffffdb10
取得ptr2記憶體位址:0x7fffffffdb18

***ptr3取得到p的值:10
**ptr3指向p記憶體位置:0x7fffffffdb0c
*ptr3指向ptr1的記憶體位置:0x7fffffffdb10
ptr3指向ptr2的記憶體位置:0x7fffffffdb18
取得ptr3記憶體位址:0x7fffffffdb20
```

上面一串三重指標看似超深澳，但轉化成下面這張圖就很好理解了，好像越解釋越複雜了，算了XD

![alt text](./static/ptr.png "***ptr")


## day20 

今天來記錄一個簡單的概念sizeof，sizeof可以幫助計算型態, 結構體, 與陣列, 與指標的大小，例如
short: 2bytes  
long: 4 bytes  
double: 8 bytes  
double arrary[20]: 160 bytes  
值得注意的是指標在32位元作業系統是4 bytes (32/8=4)到了64位元作業系統是8 bytes，由此可知若在64位元作業系統底下，指標拿來指向short反而會浪費記憶體。
雖然在C++中陣列本身就是指標，可以直接將陣列丟給指標變數不會報錯*ptr = arrary，但將陣列丟給sizeof時會自動轉換成計算整個陣列大小。如
```
int arrary[20];
int *ptr = arrary;
count << sizeof arrary <<endl;  //get 80 bytes
count << sizeof *ptr <<endl;  //get 4 bytes in 32-bits os, 32/8=4

```
昨天我們輸出結果可以看出來p的記憶體位置與\*ptr1的記憶體位置差4bytes(因為int型態佔4bytes)，而\*ptr1與\*\*ptr2與\*\*\*ptr3各差了8bytes(在64位元系統中64bits=8bytes)

```
**ptr3指向p記憶體位置:0x7fffffffdb0c
*ptr3指向ptr1的記憶體位置:0x7fffffffdb10
ptr3指向ptr2的記憶體位置:0x7fffffffdb18
取得ptr3記憶體位址:0x7fffffffdb20
```

## day21

今天繼續看指標與陣列陷入深深地不知該如何寫心得中，指標\*ptr是專門用來指向物件記憶體位置的類型。在C++中陣列跟指標有密不可分的連結，參考螞蟻書的介紹一個int v[5]陣列的結構可以用下圖表示，而陣列名稱v就是永遠指向v[0]記憶體位置的指標，因為v本身是指標所以可以直接丟給\*ptr並透過修改*ptr的記憶體位置即ptr進一步取陣列的每個元素的值，程式碼如下
```
int main(){

    int v[5] = {100,200,300,400,500}; 
    int *vPtr = v ;  //直接將陣列v丟給指標
    cout << vPtr  << endl ;               //3000
    cout << *( vPtr  + 2)  << endl ;   // 300
    cout <<  vPtr  + 2  << endl ;    //3008
    cout << ++vPtr  << endl ;          //3004，騷操作
    return 0;
}

```
值得注意的是運算子是有階級的階級高的先執行，因為\*階級高於+所以要用括號包起來，若不刮起來 \*vPtr+2會變成 v[0]+2，至於為什麼代碼寫vPtr+2 記憶體位置卻+8那是因為int佔4個bytes,2*4=8真是令人頭昏目眩的寫法。
![alt text](./static/d41.png "***ptr")


## day22
陣列名稱就是陣列第一個元素的記憶體位置，同理函數名稱也是程式碼在記憶體的第一個位置，既然有了記憶體位置當然可以有個指標指向它，這種指標又稱為函數指標(指向函數的指標變數)，函數指標主要有兩個用途，
1. 拿來呼叫函數，
2. 似於Python的把函數當參數用    

螞蟻書的範例太長不解說，所以去網路上收尋了一位高手範例與解說:  
>引用自: Bluelove1968大大
```
#include <stdio.h>
#include <iostream>
int max(int x, int y){return(x>y?x:y);}
int main(){
    int (*ptr)(int,int);
    int a, b=3, c;
    ptr = max;
    scanf("%d, %d", &a ,&b);
    c = (*ptr)(a,b);
    printf("a = %d,b = %d,max = %d",a,b,c);
    return 0;
}
```
函數名max是記憶體位置，透過ptr = max;將記憶體位址設給ptr，之後只要呼叫(\*ptr)(a,b);就可以得到max(a,b)同樣結果，函數指標好處在於函數名是寫死的，函數指標卻是活的可以指向新的函數只要參數返回值一樣就行，例如: min(a,b) 。




## day23

函數指標只要參數與返回值相同是可以隨時指向一個新的函數如前所說的max, min，當然C++作為那個站在鄙視鏈頂端的語言(汗)，當然要有更風騷的套路，函數指標+陣列迸出新滋味，比如我們有三個函數A, B , C 它們的參數都是int無法返回值，因為它們的型態都一樣自然可以用同類型的函數指標隨意指向任何一個函數
```
void (*func)( int );
func=function_A;
func(99);
func=function_B;
func(99);
func=function_C;
func(99);
```
或是玩更有趣一點，乾脆創造一個函數指標"陣列"存放三個函數，接著就像讀取陣列元素一樣呼叫函數
```
//創造一個函數指標"陣列f包含三個function，A/B/C
void (*f[ 3 ])( int ) = { function_A, function_B, function_C };
cout << "輸入0~2選擇你要執行的函數，超過程式會直接退出";
cin >> choice;
//while迴圈只要choice<=2就會執行對應的函數
while ( ( choice >= 0 ) && ( choice < 3 ) ) 
{
        (*f[ choice ])( choice ); 
        cout << "Choice your fun between 0 ~ 2, or exit under 2";
        cin >> choice;
}
```

原始碼在 day22_example

## day24

1.	9.5節提到判斷方法(predicate function)的概念，簡單來說就是許多容器類別: 如link list, queue, stack都有一個方法isEnpty()或是isFull()就是所謂的判斷方法。

2.	螞蟻書有個小優點就是美的段落的有個tips，這裡提到從安全性考量傳值呼叫的方式傳遞物件非常好，因為被呼叫的函式沒法修改原來的物件，但是大型物件的副本記憶體用量大效率差，所以用指標或參照的方式效率比較好，而傳const指標/參照可以結合兩者的優點。做法是用指向const資料的指標當參數傳入函式


* Pointer to const object
You can modify the pointer but you can't modify the object:
```
const Object* object_ptr = object1;
object_ptr = object2;                // Modify pointer, OK
object_ptr->x = 40;                  // Modify object, ERROR
```
* Const pointer to object
You can't modify the pointer but you can modify the object:
```
const Object* object_ptr = object1;
object_ptr = object2;                // Modify pointer, OK
object_ptr->x = 40;                  // Modify object, ERROR
```
ref :Constant pointers vs. pointer to constants in C and C++  


## day25 繼承與多型
本日篇幅較多包含10個檔案所以所有程式碼都擺在day25資料夾  
在寫程式時，若需定義多個類別(比如類別A、B、C)，而類別B、C擁有類別A的某些資料成員、或某些成員方法，則我們可以使用繼承讓B/C直接獲得A的public與protect部分，在C++中一般稱呼A為基本類別,B與C為衍生類別，基本類別就是Java裡的父類別而衍生類別就是子類別，C++真是一個麻煩的語言,在C++中類別分成public, protect, private三部分，繼承也分為public, protect, private inheritance，我們主要探討螞蟻書的public inheritance中關於私有資料成員如何處理的範例:

![alt text](./static/d251.png "***ptr")

如上圖所示即使是public inheritance也無法存取父類別的private member(date and method)，下面是父類別Employee的標頭檔可以看到有三個private date member，分別是firstName, lastName, socialSecurityNumber, 子類別是無法讀取的

```
// Fig. 13.13: Employee.h
// Employee abstract base class.
#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string> // C++ standard string class
using std::string;

class Employee 
{
public:
   Employee( const string &, const string &, const string & );

   void setFirstName( const string & ); // set first name
   string getFirstName() const; // return first name

   void setLastName( const string & ); // set last name
   string getLastName() const; // return last name

   void setSocialSecurityNumber( const string & ); // set SSN
   string getSocialSecurityNumber() const; // return SSN

   // pure virtual function makes Employee abstract base class
   virtual double earnings() const = 0; // pure virtual
   virtual void print() const; // virtual
private:
   string firstName;
   string lastName;
   string socialSecurityNumber;
}; // end class Employee

#endif // EMPLOYEE_H
```

類別CommissionEmployee的原始碼.cpp檔中可以發現子類別的print()方法中有一段呼叫父類別方法的代碼Employee::print();，再去點開父類別的原始碼.cpp檔會發現父類別的print()方法會再call三個成員方法getFirstName(),getLastName(), getSocialSecurityNumber()。

```
// Fig. 13.14: Employee.cpp
// Abstract-base-class Employee member-function definitions.
// Note: No definitions are given for pure virtual functions.
#include <iostream>
using std::cout;

#include "Employee.h" // Employee class definition

// constructor
Employee::Employee( const string &first, const string &last,
   const string &ssn )
   : firstName( first ), lastName( last ), socialSecurityNumber( ssn )
{
   // empty body 
} // end Employee constructor

// set first name
void Employee::setFirstName( const string &first ) 
{ 
   firstName = first;  
} // end function setFirstName

// return first name
string Employee::getFirstName() const 
{ 
   return firstName;  
} // end function getFirstName

// set last name
void Employee::setLastName( const string &last )
{
   lastName = last;   
} // end function setLastName

// return last name
string Employee::getLastName() const
{
   return lastName;   
} // end function getLastName

// set social security number
void Employee::setSocialSecurityNumber( const string &ssn )
{
   socialSecurityNumber = ssn; // should validate
} // end function setSocialSecurityNumber

// return social security number
string Employee::getSocialSecurityNumber() const
{
   return socialSecurityNumber;   
} // end function getSocialSecurityNumber

// print Employee's information
void Employee::print() const
{ 
   cout << getFirstName() << ' ' << getLastName() 
      << "\nsocial security number: " << getSocialSecurityNumber(); 
} // end function print
```

在CommissionEmployee的標頭檔可以發現確實沒有定義firstName, lastName, socialSecurityNumber三個資料成員與getFirstName(),getLastName(), getSocialSecurityNumber()三個成員方法，現在好玩的來了!如果再多一個子類別:佣金+底薪員工去繼承佣金員工CommissionEmployee會怎樣呢? 老樣子直接點開BasePlusCommissionEmployee的標頭檔與與原始碼.cpp，可以驚人的發現BasePlusCommissionEmployee的成員方法print()直接呼叫CommissionEmployee::print();也沒有去定義所需的成員方法與資料。這就是繼承的核心大量的程式碼復用reuse。  

最後點開主程式fig13_25.cpp，可以看出父類別Employee有三個子類別SalariedEmployee, HourlyEmployee, CommissionEmployee與一個孫類別BasePlusCommissionEmployee，首先創造一個Employee類別指標向量vector < Employee * > employees( 4 ); 由於四個類別都繼承自Employee所以可以直接擺到Employee類別指標向量中，接著透過for迴圈逐一呼叫print()方法成員，這裡有個Tricky，因為我們前面使用的是指標所以是必須使用->如employees[i]->print();而非.(dot) 呼叫成員方法，而回到typeid時就不再是指標所以用.(dot) 如typeid( *employees[j] ).name() << endl;  


## day26 繼承與多型

在強行別語言中我們必須先定義變數的型別，例如vector<int> a(10); 一個int向量只能放int元素否則會出現錯誤，不像python的list可以隨便放，但如果每個元素都繼承同一個父類別就可以使用如同昨天所說的vector < Employee * > employees( 4 )將子類別SalariedEmployee, HourlyEmployee, CommissionEmployee與孫類別BasePlusCommissionEmployee都丟進同一個向量。

說道多型一定要講這個老掉牙的解釋，動物會發出聲音如貓會喵喵叫狗會汪汪叫鴨子會呱呱叫，貓咪狗狗鴨子都是動物而且它們有不同的聲音，所以我們創造一個父類別Animal但並不實作say()方法，等到貓咪繼承時在去實作喵喵叫方法，狗狗與鴨子同理這就是多型。每一個人的解釋都是Animal跟Hello World同等級的存在。 在Animal的類別中我們可以將一些共同的特性實作在父類別如每個動物都要呼吸都要睡覺都有所屬的界門綱目科屬種，並將一些特殊特性在父類別進行抽象化，等到子類別時才進行實作。  

螞蟻書在這邊提出一個好玩的觀點父類別可以丟給子類別指標，但子類別卻無法丟給父類別的指標，用CommissionEmployee(父)與BasePlusCommissionEmployee(子)來說一但可以將子類別地址丟給父類別指標，那等於讓CommissionEmployee(父)也得到setBaseSalary()方法，問題CommissionEmployee(父)在創建記憶體空間時並沒有留下給setBaseSalary()方法的空間，導致記憶體複寫到其他物件。所以出現禁止這種反向繼承的規則。為避免上述問題例如下例子類別指標指向父類別是不被允許的，然而父類別指標可以指向子類別物件這就是向下轉型(down casting)。
 
```
// Fig. 13.6: fig13_06.cpp
// Aiming a derived-class pointer at a base-class object.
#include "CommissionEmployee.h"
#include "BasePlusCommissionEmployee.h"

int main()
{
   CommissionEmployee commissionEmployee( 
      "Sue", "Jones", "222-22-2222", 10000, .06 );
   BasePlusCommissionEmployee *basePlusCommissionEmployeePtr = 0;

   // aim derived-class pointer at base-class object
   // Error: a CommissionEmployee is not a BasePlusCommissionEmployee
   basePlusCommissionEmployeePtr = &commissionEmployee;
   return 0;
} // end main


```

## day27 繼承與多型

雖然有向下轉型但透過指標呼叫方法時到底會呼叫到父類別的方法還是子類別的方法呢?來講一個很容易弄錯的地方,由於子類別類直接繼承父類別的成員方法，這時想要重載父類別的方法就需要關鍵字virtual。還是直接看程式碼進行比較父類別CommissionEmployee與子類別BasePlusCommissionEmployee。

* 沒用virtual的情況    
如下fig13_05.cpp範例中將basePlusCommissionEmployee的記憶體位置透過&運算子丟給*commissionEmployeePtr指標，再透過指標呼叫print()方法，由於類別型態是commissionEmployee故C++編譯器會直接呼叫父類別的方法而非子類別的方法。    
```
// Fig. 13.5: fig13_05.cpp

int main()
{
   // ....省略若干程式碼

   commissionEmployeePtr = &basePlusCommissionEmployee;
   cout << "\n\n\nCalling print with base-class pointer to "
      << "derived-class object\ninvokes base-class print "
      << "function on that derived-class object:\n\n";
   commissionEmployeePtr->print(); // invokes base-class print
   cout << endl;
   return 0;


}

```
```
//Result
Calling print with base-class pointer to derived-class object
invokes base-class print function on that derived-class object:
commission employee: Bob Lewis
social security number: 333-33-3333
gross sales: 5000.00
commission rate: 0.04 

```

* 用virtual的情況   

上面的例子顯示了一個重要的問題，透過父類別指標即使指向子類別記憶體地址也呼叫不到子類別的方法，這樣就無法實踐"多型"的概念，這時候就需要透過關鍵字 virtual讓編譯器能重新寫入(override)，動態的連結到子類別的方法以實現多型的概念，有了virtual程式就可以根據物件實際型別而非指標型別來選擇正確的方法(Method)，因為不是在編譯時就決定而是在執行期間動態決定所以又稱為dynamic binding或late binding。  

其使用方式也很簡單在需要dynamic binding的方法前加上virtual關鍵字即可，virtual關鍵字具有強制繼承特性不管子類別的方法前有無virtual只要父類別有virtual子類別與孫類別的該方法都會強制轉換成virtual方法。  



## day28 錯誤處理
講一個經典案例除以0，首先創造一個DivideByZeroEXception類別繼承runtime_error。在C++中double除以0並不會報錯，所以主動用throw丟出DivideByZeroEXception，並在catch段落呼叫what()方法顯示錯誤原因，最外面用while()產生一個無限迴圈

```
while ( cin >> number1 >> number2 ) 
{
   // try block contains code that might throw exception
   // and code that should not execute if an exception occurs
   try 
   {
      result = quotient( number1, number2 );
      cout << "The quotient is: " << result << endl;
   } // end try

   // exception handler handles a divide-by-zero exception
   catch ( DivideByZeroException &divideByZeroException ) 
   {
      cout << "Exception occurred: " 
         << divideByZeroException.what() << endl;
   } // end catch

   cout << "\nEnter two integers (end-of-file to end): ";
} // end while
```
詳細範例見Fig. 16.1: DivideByZeroException.h與Fig. 16.2: Fig16_02.cpp

## day29
在64位元系統指標是64/8 = 8bytes，而double也是8bytes若指標指向更小的型態如int就不合算了，相反的指向物件就很適合，但因為指標與傳值不同，指標會修改到被指向的目標，如果想避免修改到目標可以使用常數指標(const *Ptr)。
與C不同C++除了傳值與傳址還有一個傳參考(pass by reference)，在傳址中我們常見到一個符號&其目的是取出記憶體位置，但在傳參考中&只是告訴complier現在是傳參考，另外參考只是一個別名(allies)而已，怎麼說呢?以下兩個swap範例中我們會發現參考(reference)並沒有自己的記憶體位置而是直接使用傳進來的變數的記憶體位置，相反的指標版本的swap多出了2個記憶體位置。

* SWAP 傳參考版
```
#include <stdio.h>
#include <stdlib.h>

void swap(int &x, int &y) {
    printf("x,y記憶體位置:%p,%p\n", &x, &y);
    int tmp = x;
    x = y;
    y = tmp;
}

int main(){
    
    int a = 3;
    int b = 5;

    printf("a:%d,b:%d\n", a, b);
    printf("a,b記憶體位置:%p,%p\n", &a, &b);
    swap(a, b);
    printf("a:%d,b:%d\n", a, b);
    return 1;
}
```
```
//result
a:3,b:5
a,b記憶體位置:0x7fffffffdb10, 0x7fffffffdb14
x,y記憶體位置:0x7fffffffdb10, 0x7fffffffdb14
a:5,b:3
```

* SWAP 傳指標版
```
#include <stdio.h>
#include <stdlib.h>

void swap(int *x, int *y) {

    printf("x,y記憶體位置:%p,%p\n", &x, &y);
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

int main(){
    
    int a = 3;
    int b = 5;

    printf("a:%d,b:%d\n", a, b);
    printf("a,b記憶體位置:%p,%p\n", &a, &b);
    swap(&a, &b);
    printf("a:%d,b:%d\n", a, b);

    return 1;
}
```

```
//result
a:3,b:5
a,b記憶體位置:0x7fffffffdb20, 0x7fffffffdb24
x,y記憶體位置:0x7fffffffdaf8, 0x7fffffffdaf0
a:5,b:3
```



## day30 
auto_ptr常用的程式設計習慣是動態配置記憶體就是將某記憶體位置丟給指標後當在不需使用時才用delete手動回收，但如果在這之前發生例外，就會產生記憶體洩漏(Memory leak)。C++標準函式庫中有提供樣板auto_ptr處理這種狀況, 

```
// Fig. 16.10: Fig16_10.cpp
// Demonstrating auto_ptr.
#include <iostream>
using std::cout;
using std::endl;

#include <memory>
using std::auto_ptr; // auto_ptr class definition

#include "Integer.h"

// use auto_ptr to manipulate Integer object
int main()
{
   cout << "Creating an auto_ptr object that points to an Integer\n";

   // "aim" auto_ptr at Integer object
   auto_ptr< Integer > ptrToInteger( new Integer( 7 ) );

   cout << "\nUsing the auto_ptr to manipulate the Integer\n";
   ptrToInteger->setInteger( 99 ); // use auto_ptr to set Integer value

   // use auto_ptr to get Integer value
   cout << "Integer after setInteger: " << ( *ptrToInteger ).getInteger()
      << "\n\nTerminating program" << endl;
   return 0;
}  // end main
```

auto_ptr< Integer > ptrToInteger( new Integer( 7 ) ); 
// ptrToInteger是個auto_ptr< Integer >的樣板指標指向Integer物件，一開始透過建構子初始化值為7
ptrToInteger->setInteger( 99 );
//這邊是透過auto_ptr操作Integer的setInteger()方法故使用->箭頭符號
( \*ptrToInteger ).getInteger()
//神奇操作來了，對指標auto_ptr進行*運算(取值運算)取回了Integer物件故這裡是用.(dot)

// *取值運算
// & 取址運算

Integer的原始碼請參考: day30_example https://gitlab.com/Ted_Griffin771/cpp-book-report/-/tree/master/examples/day30_example
